TCCpp
=====

TCCpp is a C++ header-only library that makes it easier to work with TinyCC and C++-classes. This is also useful if you do **not** intend to work with classes as this includes a TinyCC-wrapper to handle it more easily. TinyCC is a library to compile C-code on the fly and makes it therefore easy to embed into your application. This header does not make the TinyCC compile C++.

C++11
-----

In some cases I also included some neat **optional** things from C++11.

Dependencies
------------

*   TinyCC - [Projectpage](http://www.bellard.org/tcc/) - Tested with 0.9.26 and on linux using clang and on Windows 7 with gcc/MinGW

Usage example
-------------

```
#!c++

// includes C++11 code

#include <iostream>
#include <sstream>
#include <functional>

#include "TCC.hpp"

int strToInt(char* text)
{
    int result = 0;
    if (std::istringstream(text) >> result)
        return result;
    return 0;
}

const char* code =
    "int i1, i2;"
    "void test()"
    "{"
    "    i1 = StringToInt(\"123\");"
    "    i2 = StringToInt(\"Oh noze\");"
    "}";

int main(int, char**)
{
    TCC::Handle h;
    
    if (!h)
	{
        std::cout << "Could not create tcc state\n";
        return 1;
    }

    // Memory means: use it directly in your code
	h.SetOutputType(TCC::Handle::Output::Memory);

    if (!h.CompileString(code))
	{
        std::cout << "Could not compile given code\n";
        return 1;
    }

    // add function
	h.AddSymbol("StringToInt", &strToInt);

    // relocate the code
    // needed to have the code in memory, maybe this will get automated
	if (h.Relocate() < 0)
	{
		std::cout << "Could not relocate the code\n";
		return 1;
	}

    // get function
    // either c++11
    std::function<void()> test11 = TCC::Helper(h).GetFunction<void()>("test");
    test11();
    // or c++99
    void (*test)() = h.GetSymbol<void(*)()>("test");
    (*test)();

    int* i1 = h.GetSymbol<int*>("i1");
    int* i2 = h.GetSymbol<int*>("i2");
    
    std::cout << "i1 = " << (i1?*i1:0) << "\n";
    std::cout << "i2 = " << (i2?*i2:0) << "\n";
    
    return 0;
}
```

License
-------

Copyright (c) 2014 Joshua Behrens

This software is provided 'as-is', without any express or implied warranty. In no event will the authors be held liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose, including commercial applications, and to alter it and redistribute it freely, subject to the following restrictions:

*   The origin of this software must not be misrepresented; you must not claim that you wrote the original software. If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.

*   Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.

*   This notice may not be removed or altered from any source distribution.

Based on the [zlib-license](http://zlib.net/zlib_license.html)