##
## Auto Generated makefile by CodeLite IDE
## any manual changes will be erased      
##
## Debug
ProjectName            :=Test2
ConfigurationName      :=Debug
WorkspacePath          := "/home/joshua/Git/tccpp/codelite"
ProjectPath            := "/home/joshua/Git/tccpp/codelite/Test2"
IntermediateDirectory  :=../../bin
OutDir                 := $(IntermediateDirectory)
CurrentFileName        :=
CurrentFilePath        :=
CurrentFileFullPath    :=
User                   :=joshua
Date                   :=09/25/14
CodeLitePath           :="/home/joshua/.codelite"
LinkerName             :=/usr/bin/clang++ 
SharedObjectLinkerName :=/usr/bin/clang++ -shared -fPIC
ObjectSuffix           :=.o
DependSuffix           :=.o.d
PreprocessSuffix       :=.i
DebugSwitch            :=-g 
IncludeSwitch          :=-I
LibrarySwitch          :=-l
OutputSwitch           :=-o 
LibraryPathSwitch      :=-L
PreprocessorSwitch     :=-D
SourceSwitch           :=-c 
OutputFile             :=$(IntermediateDirectory)/$(ProjectName)-d
Preprocessors          :=
ObjectSwitch           :=-o 
ArchiveOutputSwitch    := 
PreprocessOnlySwitch   :=-E
ObjectsFileList        :="Test2.txt"
PCHCompileFlags        :=
MakeDirCommand         :=mkdir -p
LinkOptions            :=  
IncludePath            :=  $(IncludeSwitch). $(IncludeSwitch). $(IncludeSwitch)../../src/include/ 
IncludePCH             := 
RcIncludePath          := 
Libs                   := $(LibrarySwitch)tcc $(LibrarySwitch)dl 
ArLibs                 :=  "tcc" "dl" 
LibPath                := $(LibraryPathSwitch). 

##
## Common variables
## AR, CXX, CC, AS, CXXFLAGS and CFLAGS can be overriden using an environment variables
##
AR       := /usr/bin/ar rcu
CXX      := /usr/bin/clang++ 
CC       := /usr/bin/clang 
CXXFLAGS :=  -g -std=c++11 -Wall -O0 $(Preprocessors)
CFLAGS   :=  -g -O0 -Wall $(Preprocessors)
ASFLAGS  := 
AS       := /usr/bin/as 


##
## User defined environment variables
##
CodeLiteDir:=/usr/share/codelite
Objects0=$(IntermediateDirectory)/test_test2.cpp$(ObjectSuffix) 



Objects=$(Objects0) 

##
## Main Build Targets 
##
.PHONY: all clean PreBuild PrePreBuild PostBuild
all: $(OutputFile)

$(OutputFile): $(IntermediateDirectory)/.d $(Objects) 
	@$(MakeDirCommand) $(@D)
	@echo "" > $(IntermediateDirectory)/.d
	@echo $(Objects0)  > $(ObjectsFileList)
	$(LinkerName) $(OutputSwitch)$(OutputFile) @$(ObjectsFileList) $(LibPath) $(Libs) $(LinkOptions)

$(IntermediateDirectory)/.d:
	@test -d ../../bin || $(MakeDirCommand) ../../bin

PreBuild:


##
## Objects
##
$(IntermediateDirectory)/test_test2.cpp$(ObjectSuffix): ../../src/test/test2.cpp $(IntermediateDirectory)/test_test2.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/joshua/Git/tccpp/src/test/test2.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/test_test2.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/test_test2.cpp$(DependSuffix): ../../src/test/test2.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/test_test2.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/test_test2.cpp$(DependSuffix) -MM "../../src/test/test2.cpp"

$(IntermediateDirectory)/test_test2.cpp$(PreprocessSuffix): ../../src/test/test2.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/test_test2.cpp$(PreprocessSuffix) "../../src/test/test2.cpp"


-include $(IntermediateDirectory)/*$(DependSuffix)
##
## Clean
##
clean:
	$(RM) ../../bin/*$(ObjectSuffix)
	$(RM) ../../bin/*$(DependSuffix)
	$(RM) $(OutputFile)
	$(RM) "../.build-debug/Test2"


