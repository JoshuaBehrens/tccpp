##
## Auto Generated makefile by CodeLite IDE
## any manual changes will be erased      
##
## Debug
ProjectName            :=Test1
ConfigurationName      :=Debug
WorkspacePath          := "/home/joshua/Git/tccpp/codelite"
ProjectPath            := "/home/joshua/Git/tccpp/codelite/Test1"
IntermediateDirectory  :=../../bin
OutDir                 := $(IntermediateDirectory)
CurrentFileName        :=
CurrentFilePath        :=
CurrentFileFullPath    :=
User                   :=joshua
Date                   :=09/20/14
CodeLitePath           :="/home/joshua/.codelite"
LinkerName             :=clang++
SharedObjectLinkerName :=clang++ -shared -fPIC
ObjectSuffix           :=.o
DependSuffix           :=
PreprocessSuffix       :=.o.i
DebugSwitch            :=-gstab
IncludeSwitch          :=-I
LibrarySwitch          :=-l
OutputSwitch           :=-o 
LibraryPathSwitch      :=-L
PreprocessorSwitch     :=-D
SourceSwitch           :=-c 
OutputFile             :=$(IntermediateDirectory)/$(ProjectName)-d
Preprocessors          :=
ObjectSwitch           :=-o 
ArchiveOutputSwitch    := 
PreprocessOnlySwitch   :=-E 
ObjectsFileList        :="Test1.txt"
PCHCompileFlags        :=
MakeDirCommand         :=mkdir -p
LinkOptions            :=  
IncludePath            :=  $(IncludeSwitch). $(IncludeSwitch). $(IncludeSwitch)../../src/include/ 
IncludePCH             := 
RcIncludePath          := 
Libs                   := $(LibrarySwitch)tcc $(LibrarySwitch)dl 
ArLibs                 :=  "tcc" "dl" 
LibPath                := $(LibraryPathSwitch). 

##
## Common variables
## AR, CXX, CC, AS, CXXFLAGS and CFLAGS can be overriden using an environment variables
##
AR       := ar rcus
CXX      := clang++
CC       := clang
CXXFLAGS :=  -g -std=c++11 -Wall -O0 $(Preprocessors)
CFLAGS   :=  -g -O0 -Wall $(Preprocessors)
ASFLAGS  := 
AS       := llvm-as


##
## User defined environment variables
##
CodeLiteDir:=/usr/share/codelite
Objects0=$(IntermediateDirectory)/test_test1$(ObjectSuffix) 



Objects=$(Objects0) 

##
## Main Build Targets 
##
.PHONY: all clean PreBuild PrePreBuild PostBuild
all: $(OutputFile)

$(OutputFile): $(IntermediateDirectory)/.d $(Objects) 
	@$(MakeDirCommand) $(@D)
	@echo "" > $(IntermediateDirectory)/.d
	@echo $(Objects0)  > $(ObjectsFileList)
	$(LinkerName) $(OutputSwitch)$(OutputFile) @$(ObjectsFileList) $(LibPath) $(Libs) $(LinkOptions)

$(IntermediateDirectory)/.d:
	@test -d ../../bin || $(MakeDirCommand) ../../bin

PreBuild:


##
## Objects
##
$(IntermediateDirectory)/test_test1$(ObjectSuffix): ../../src/test/test1.cpp 
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/joshua/Git/tccpp/src/test/test1.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/test_test1$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/test_test1$(PreprocessSuffix): ../../src/test/test1.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/test_test1$(PreprocessSuffix) "../../src/test/test1.cpp"

##
## Clean
##
clean:
	$(RM) $(IntermediateDirectory)/test_test1$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/test_test1$(DependSuffix)
	$(RM) $(IntermediateDirectory)/test_test1$(PreprocessSuffix)
	$(RM) $(OutputFile)
	$(RM) "../.build-debug/Test1"


