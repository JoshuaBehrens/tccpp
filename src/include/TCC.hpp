#ifndef _TCC_HPP
	#define _TCC_HPP

	#include <iostream>
	#include <string>
	#include <exception>

	extern "C"
	{
		#include <libtcc.h>
	}
	
namespace TCC
{
	class Handle
	{
	public:
		Handle()
		{
			state = tcc_new();
		}
		// move only, no copy
		Handle(const Handle& s) = delete;
		Handle& operator =(const Handle& s) = delete;
		Handle(Handle&& s) : state(nullptr)
		{
			destroy();
			state = s.state;
			s.state = nullptr;
		}
		Handle& operator =(Handle&& s)
		{
			destroy();
			state = s.state;
			s.state = nullptr;
			return *this;
		}
		~Handle()
		{
			destroy();
		}
		
		/* set CONFIG_TCCDIR at runtime */
		void SetTCCPath(const std::string& path)
		{
			if (state)
			{
				tcc_set_lib_path(state, path.c_str());
			}
		}
		/* set error/warning display callback */
		// void tcc_set_error_func(TCCState *s, void *error_opaque,
		// void (*error_func)(void *opaque, const char *msg));

		/* set options as from command line (multiple supported) */
		int SetOption(const std::string& option)
		{
			if (state)
			{
				return tcc_set_options(state, option.c_str());
			}
			return -1;
		}
		/* preprocessor */

		int AddIncludePath(const std::string& path, const bool& system = false)
		{
			if (state)
			{
				if (system)
					return tcc_add_sysinclude_path(state, path.c_str());
				else
					return tcc_add_include_path(state, path.c_str());
			}
			return -1;
		}
		
		/* define preprocessor symbol 'sym'. Can put optional value */
		void Define(const std::string& name, const std::string& value = "")
		{
			if (state)
			{
				tcc_define_symbol(state, name.c_str(), value.c_str());
			}
		}
		
		/* undefine preprocess symbol 'sym' */
		void Undefine(const std::string& name)
		{
			if (state)
			{
				tcc_undefine_symbol(state, name.c_str());
			}
		}

		/* add a file (C file, dll, object, library, ld script). Return -1 if error. */
		bool AddFile(const std::string& file)
		{
			if (state)
			{
				return tcc_add_file(state, file.c_str()) > -1;
			}
			return false;
		}
		
		/* compile a string containing a C source. Return -1 if error. */
		bool CompileString(const std::string& code)
		{
			if (state)
			{
				return tcc_compile_string(state, code.c_str()) > -1;
			}
			return false;
		}

		enum Output
		{
			/* output will be run in memory (default) */
			Memory = 0,
			/* executable file */
			Executable = 1,
			/* dynamic library */
			DynamicLibrary = 2,
			/* object file */
			ObjectFile = 3,
			/* only preprocess (used internally) */
			Preprocess = 4
		};
		
		/* set output type. MUST BE CALLED before any compilation */
		bool SetOutputType(const Output& out)
		{
			if (state)
			{
				return tcc_set_output_type(state, (int)out) > -1;
			}
			return false;
		}

		/* equivalent to -Lpath option */
		bool AddLibraryPath(const std::string& path)
		{
			if (state)
			{
				return tcc_add_library_path(state, path.c_str()) > -1;
			}
			return false;
		}
		
		/* the library name is the same as the argument of the '-l' option */
		bool AddLibrary(const std::string& path)
		{
			if (state)
			{
				return tcc_add_library(state, path.c_str()) > -1;
			}
			return false;
		}
		
		/* add a symbol to the compiled program */
		template <typename T> bool AddSymbol(const std::string& name, T symbol)
		{
			if (state)
			{
				return tcc_add_symbol(state, name.c_str(), reinterpret_cast<void*>(symbol)) > -1;
			}
			return false;
		}
		
		/* output an executable, library or object file. DO NOT call tcc_relocate() before. */	
		bool OutputFile(const std::string& filename)
		{
			if (state)
			{
				return tcc_output_file(state, filename.c_str()) > -1;
			}
			return false;
		}
		
		/* link and run main() function and return its value. DO NOT call tcc_relocate() before. */
		int RunMain(int argc, char** argv)
		{
			if (state)
			{
				return tcc_run(state, argc, argv);
			}
			return -1;
		}

		/* do all relocations (needed before using tcc_get_symbol()) */
		/* possible values for 'ptr':
		   - TCC_RELOCATE_AUTO : Allocate and manage memory internally (1)
		   - NULL              : return required memory size for the step below
		   - memory address    : copy code to memory passed by the caller
		   returns -1 if error. */
		int Relocate(void *ptr = (void*)1)
		{
			if (state)
			{
				return tcc_relocate(state, ptr);
			}
			return -1;
		}
		
		/* return symbol value or NULL if not found */
		template <typename T> T GetSymbol(const std::string& name)
		{
			if (state)
			{
				void* symbol = tcc_get_symbol(state, name.c_str());
				if (symbol)
					return reinterpret_cast<T>(symbol);
			}
			return nullptr;
		}
		
		operator bool() const
		{
			return state != nullptr;
		}
	private:
		TCCState *state = nullptr;
		void destroy()
		{
			if (state)
			{
				tcc_delete(state);
				state = nullptr;
			}
		}
	};

	class Helper
	{
	public:
		Helper() = delete;
		Helper(Handle& h) : handle(h) {}
		Helper(Helper&& h) = delete;
		Helper& operator=(Helper& h) = delete;
		Helper& operator=(Helper&& h) = delete;
		
		template <typename T> std::function<T> GetFunction(const std::string& name)
		{
			std::function<T> result;
			if (handle)
			{
				result = handle.GetSymbol<T*>(name);
			}
			return result;
		}

		operator bool() const
		{
			return handle;
		}
	private:
		Handle& handle;
	};
}

	#define ON_NULL_PTR(TYPE, FUNCNAME) throw std::runtime_error("Instance of " TCC_STRING(TYPE) " is null in call of " FUNCNAME)
	
	#define TCC_STRING(X) #X
	#define TCC_CAST(TYPE, VAR) reinterpret_cast<TYPE>(VAR)
	
	#define TCC_DEFINE_CTOR(TYPE) void TYPE##_new(void** ptr)
	#define TCC_IMPL_CTOR(TYPE) TCC_DEFINE_CTOR(TYPE) { TYPE** inst = TCC_CAST(TYPE**, ptr); *inst = new TYPE(); }
	#define TCC_BIND_CTOR(HANDLE, TYPE) HANDLE.AddSymbol(TCC_STRING(TYPE##_new), &TYPE##_new)
	
	#define TCC_DEFINE_DTOR(TYPE) void TYPE##_delete(void** ptr)
	#define TCC_IMPL_DTOR(TYPE) TCC_DEFINE_DTOR(TYPE) { TYPE** inst = TCC_CAST(TYPE**, ptr); if (*inst) { delete *inst; *inst = nullptr; } else { ON_NULL_PTR(TYPE, TCC_STRING(TYPE##_delete)); } }
	#define TCC_BIND_DTOR(HANDLE, TYPE) HANDLE.AddSymbol(TCC_STRING(TYPE##_delete), &TYPE##_delete)
	
	#define TCC_DEFINE_VOID(TYPE, FUNC) void TYPE##_##FUNC (void** ptr)
	#define TCC_IMPL_VOID(TYPE, FUNC) TCC_DEFINE_VOID(TYPE, FUNC) { TYPE** inst = TCC_CAST(TYPE**, ptr); if (*inst) (*inst)->FUNC(); else { ON_NULL_PTR(TYPE, TCC_STRING(TYPE##_##FUNC)); } }
	#define TCC_BIND_VOID(HANDLE, TYPE, FUNC) HANDLE.AddSymbol(TCC_STRING(TYPE##_##FUNC), &TYPE##_##FUNC)
	
	#define TCC_DEFINE_VOID_N(TYPE, FUNC, ...) void TYPE##_##FUNC (void** ptr,__VA_ARGS__)
	#define TCC_IMPL_VOID_N(TYPE, FUNC, ARGS, ...) TCC_DEFINE_VOID_N(TYPE, FUNC,__VA_ARGS__) { TYPE** inst = TCC_CAST(TYPE**, ptr); if (*inst) (*inst)->FUNC ARGS; else { ON_NULL_PTR(TYPE, TCC_STRING(TYPE##_##FUNC)); } }
	
	#define TCC_DEFINE_FUNC(TYPE, RET, FUNC) RET TYPE##_##FUNC (void** ptr)
	#define TCC_IMPL_FUNC(TYPE, RET, FUNC) TCC_DEFINE_FUNC(TYPE, RET, FUNC) { TYPE** inst = TCC_CAST(TYPE**, ptr); if (*inst) return (*inst)->FUNC(); else { ON_NULL_PTR(TYPE, TCC_STRING(TYPE##_##FUNC)); } }
	#define TCC_BIND_FUNC(HANDLE, TYPE, FUNC) TCC_BIND_VOID(HANDLE, TYPE, FUNC)
	
	#define TCC_DEFINE_FUNC_N(TYPE, RET, FUNC, ...) RET TYPE##_##FUNC (void** ptr,__VA_ARGS__)
	#define TCC_IMPL_FUNC_N(TYPE, RET, FUNC, ARGS, ...) TCC_DEFINE_FUNC_N(TYPE, RET, FUNC,__VA_ARGS__) { TYPE** inst = TCC_CAST(TYPE**, ptr); if (*inst) return (*inst)->FUNC ARGS; else { ON_NULL_PTR(TYPE, TCC_STRING(TYPE##_##FUNC)); } }

#endif // _TCC_HPP