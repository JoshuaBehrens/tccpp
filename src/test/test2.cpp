#include <functional>
#include <cstdio>

#include "TCC.hpp"

const char* code =
	"#include <stdio.h>"
    "void test()"
    "{"
    "    void * count;"
    "    Counter_new(&count);"
    "    Counter_Increment(&count);"
    "    Counter_Decrement(&count);"
    "    Counter_Increment(&count);"
    "    Counter_Decrement(&count);"
	"    printf(\"%d\\n\", Counter_GetValue(&count));"
    "    Counter_Increment(&count);"
    "    Counter_DecrementBy(&count, 2);"
    "    Counter_delete(&count);"
    "    Counter_delete(&count);"
    "}";
	
class Counter
{
public:
	Counter(int count = 0) : c(count)
	{
		printf("new instance %d\n", c);
	}
	~Counter()
	{
		printf("delete instance %d\n", c);
	}
	
	void Increment()
	{
		printf("inc to %d\n", ++c);
	}
	void Decrement()
	{
		printf("dec to %d\n", --c);
	}
	void IncrementBy(int i)
	{
		printf("inc to %d\n", c+=i);
	}
	void DecrementBy(int i)
	{
		printf("dec to %d\n", c-=i);
	}
	int GetValue()
	{
		return c;
	}
private:
	int c;
};

TCC_IMPL_CTOR(Counter)
TCC_IMPL_DTOR(Counter)
TCC_IMPL_VOID(Counter, Increment)
TCC_IMPL_VOID(Counter, Decrement)
TCC_IMPL_VOID_N(Counter, IncrementBy,(arg1), int arg1)
TCC_IMPL_VOID_N(Counter, DecrementBy,(arg1), int arg1)
TCC_IMPL_FUNC(Counter, int, GetValue)

int main(int, char**)
{
    TCC::Handle h;

    if (!h)
	{
        std::cout << "Could not create tcc state\n";
        return 1;
    }

    // Memory means: use it directly in your code
	h.SetOutputType(TCC::Handle::Output::Memory);

    if (!h.CompileString(code))
	{
        std::cout << "Could not compile given code\n";
        return 1;
    }

    // add function
	TCC_BIND_CTOR(h,Counter);
	TCC_BIND_DTOR(h,Counter);
	TCC_BIND_VOID(h,Counter,Increment);
	TCC_BIND_VOID(h,Counter,Decrement);
	TCC_BIND_VOID(h,Counter,IncrementBy);
	TCC_BIND_VOID(h,Counter,DecrementBy);
	TCC_BIND_FUNC(h,Counter,GetValue);

    // relocate the code
	if (h.Relocate() < 0)
	{
		std::cout << "Could not relocate the code\n";
		return 1;
	}

    // get function
    // either c++11
    std::function<void()> test11 = TCC::Helper(h).GetFunction<void()>("test");
    test11();

	std::cin.get();

    return 0;
}