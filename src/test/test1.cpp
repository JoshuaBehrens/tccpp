#include <iostream>
#include <sstream>
#include <functional>

#include "TCC.hpp"

int strToInt(char* text)
{
    int result = 0;
    if (std::istringstream(text) >> result)
        return result;
    return 0;
}

const char* code =
    "int i1, i2;"
    "void test()"
    "{"
    "    i1 = StringToInt(\"123\");"
    "    i2 = StringToInt(\"Oh noze\");"
    "}";

int main(int, char**)
{
    TCC::Handle h;
    
    if (!h)
	{
        std::cout << "Could not create tcc state\n";
        return 1;
    }

    // Memory means: use it directly in your code
	h.SetOutputType(TCC::Handle::Output::Memory);

    if (!h.CompileString(code))
	{
        std::cout << "Could not compile given code\n";
        return 1;
    }

    // add function
	h.AddSymbol("StringToInt", &strToInt);

    // relocate the code
	if (h.Relocate() < 0)
	{
		std::cout << "Could not relocate the code\n";
		return 1;
	}

    // get function
    // either c++11
    std::function<void()> test11 = TCC::Helper(h).GetFunction<void()>("test");
    test11();
    // or c++99
    void (*test)() = h.GetSymbol<void(*)()>("test");
    (*test)();

    int* i1 = h.GetSymbol<int*>("i1");
    int* i2 = h.GetSymbol<int*>("i2");
    
    std::cout << "i1 = " << (i1?*i1:0) << "\n";
    std::cout << "i2 = " << (i2?*i2:0) << "\n";
    
    return 0;
}